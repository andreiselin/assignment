"use strict";

var app = angular.module("app", ['ngAnimate']);


    //==============//
    //  controller  //
    //==============//


(()=>{
    app.controller("MyController", ["MyFactory", "$scope", "$filter", (MyFactory, $scope, $filter)=>{


        $scope.bids    = [];
        $scope.asks    = [];
        $scope.matches = [];
        $scope.actionToDo = "start";


        // ==================================== //


        $scope.toggle = function(){
            MyFactory[$scope.actionToDo]();
            $scope.actionToDo = $scope.actionToDo === "start" ? "pause" : "start";
        };


        // ==================================== //


        $scope.$on("order.new", function(event, newOrders){

            console.log (JSON.stringify(newOrders));

            // first push all the new orders to make them matchable with olds
            newOrders.forEach(order=>{
                order.side === "ask" ? $scope.asks.push(order) : $scope.bids.push(order);
            });

            // then collecting all the matches for new orders
            var _newPriceMatches = [];
            newOrders.forEach(order=>{
                var _orderPriceMatches = _testNewOrder(order);
                _newPriceMatches = _newPriceMatches.concat(_orderPriceMatches);
            });

            // then prioritizing matches
            var _matchQueue = _sortNewMatches(_newPriceMatches);

            // then calculating their volumes
            if (_matchQueue){
                _processNewMatches(_matchQueue);
            }

            //var _prioritizedNewMatches = _sortNewMatches($scope.matches);

        });


        // ==================================== //


        function _processNewMatches(matchQueue){
            matchQueue.forEach(match=>{
                // is volume enough ?
                if (match.bid.quantity>0 && match.ask.quantity>0){
                    match.quantity = Math.min(match.bid.quantity, match.ask.quantity);
                    match.bid.quantity -= match.quantity;
                    match.ask.quantity -= match.quantity;
                    match.number = $scope.matches.length+1;
                    $scope.matches.push(match);

                    console.log("== It's a match! == bid: "+match.bid.number+", ask: "+match.ask.number+" vol: "+match.quantity);

                    ['ask','bid'].forEach(orderType=>{
                        if (match[orderType].quantity === 0){
                            var orders = $scope[orderType+"s"];
                            orders.splice(orders.indexOf(match[orderType]), 1);
                        }
                    });
                }
            });
        }


        // ==================================== //


        function _sortNewMatches(inputMatches){
            if (!inputMatches) {
                return null;
            } else {
                return $filter('orderBy')(inputMatches, ['-price','bid.number','ask.number']);
            }
        }


        // ==================================== //


        function _testNewOrder (order){
            var _orderPriceMatches = [];

            function _testAndPush(bid, ask){
                // console.log(bid, ask);
                if (bid.price >= ask.price){
                    _orderPriceMatches.push({
                        price: (bid.price + ask.price) / 2,
                        number: 0,   // will be set later
                        quantity: 0, // will be calculated later
                         bidNumber: bid.number,
                         askNumber: ask.number,
                        bid: bid,
                        ask: ask,
                        time: new Date().toLocaleTimeString()
                    });
                }
            }

            var executeAppropriate = {
                bid: (bid)=>{ $scope.asks.forEach( ask => _testAndPush(bid, ask) ); },
                ask: (ask)=>{ $scope.bids.forEach( bid => _testAndPush(bid, ask) ); }
            }[order.side](order);

            return _orderPriceMatches;
        }


        // ==================================== //


    }]); // - / controller
})();


    //===========//
    //  factory  //
    //===========//


(()=>{
    app.factory("MyFactory", ["$http", "$interval", "$rootScope", ($http, $interval, $rootScope)=>{

        var _fieldsLoaded = 0;
        var _interval = null;


        return {
            start: function(){
                _interval = $interval (function(){
                    _fieldsLoaded+=1;
                    if (_fieldsLoaded>100){
                        $interval.cancel(_interval);
                    }else{
                        $http.get("http://localhost:9888/listOrders?start="+_fieldsLoaded+"&size=1").then(function(res){
                            $rootScope.$broadcast("order.new", res.data);
                        });
                    }
                }, 500);
            },
            pause: function(){
                $interval.cancel(_interval);
            }
        }
    }]);
})();